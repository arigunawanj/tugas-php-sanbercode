@extends('layouts.template')

@section('title', 'Edit Data Cast')

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Tambah Data Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{ route('cast.update', $cast->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama</label>
                <input type="text" class="form-control" id="exampleInputEmail1" value="{{ $cast->nama }}" name="nama" placeholder="Masukkan Nama">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Umur</label>
                <input type="number" class="form-control" id="exampleInputPassword1" value="{{ $cast->umur }}" name="umur" placeholder="Masukkan Umur">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Bio</label>
                <input type="text" class="form-control" id="exampleInputPassword1" value="{{ $cast->bio }}" name="bio" placeholder="Masukkan Bio">
            </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
@endsection
