@extends('layouts.template')

@section('title', 'Detail Data Cast')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Detail Data Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Detail</th>
                    <th>Isi Data</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Nama Cast</td>
                    <td>{{ $cast->nama}}</td>
                </tr>
                <tr>
                    <td>Umur Cast</td>
                    <td>{{ $cast->umur }}</td>
                </tr>
                <tr>
                    <td>Bio</td>
                    <td>{{ $cast->bio }}</td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection
