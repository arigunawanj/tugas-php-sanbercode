@extends('layouts.template')

@section('title', 'Cast')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Data Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <a href="{{ route('cast.create') }}" class="btn btn-primary mb-3">Tambah Data</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th >#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cast as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>{{ $item->bio }}</td>
                    <td class="d-flex">
                        <a href="{{ route('cast.edit', $item->id) }}" class="btn btn-primary">Edit</a>
                        <a href="{{ route('cast.show', $item->id) }}" class="btn btn-warning ml-2">Detail</a>
                        <form action="{{ route('cast.destroy', $item->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger ml-2" type="submit" >Delete</button>
                        </form>
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>

@endsection
