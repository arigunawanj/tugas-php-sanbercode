<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = DB::table('casts')->get();
        return view('cast', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = DB::table('casts')->get();
        return view('cast-add', compact('cast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'nama'=> 'required',
            'umur'=> 'required',
            'bio'=> 'required',

            ]);

            $cast = DB::table('casts')->insert([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
            return redirect('cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = Cast::findOrFail($id);
        return view('cast-show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('cast-edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cast = Cast::findOrFail($id);
        $validator = $request->validate([
            'nama'=> 'required',
            'umur'=> 'required',
            'bio'=> 'required',

        ]);

        $cast = DB::table('casts')
        ->where('id', $id)
        ->update([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"]
        ]);
        return redirect('cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cast = DB::table('casts')->where('id', $id)->delete();;
        return redirect('cast');
    }
}
