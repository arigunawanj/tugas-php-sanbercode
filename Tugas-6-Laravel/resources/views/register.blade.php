<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up | Ari Gunawan Jatmiko</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label for="">First Name : </label>
        <br>
        <input type="text" name="fname" id="">
        <br><br>

        <label for="">Last Name :</label>
        <br>
        <input type="text" name="lname" id="">
        <br><br>
        <label for="">Gender :</label>
        <br><br>

        <input type="radio" name="gender" id="">
        <label for="">Male</label>
        <br>
        <input type="radio" name="" id="">
        <label for="">Female</label>
        <br>
        <input type="radio" name="" id="">
        <label for="">Other</label>

        <br><br>

        <label for="">Nationality :</label>
        <select name="national" id="">
            <option value="">Indonesia</option>
            <option value="">Thailand</option>
            <option value="">Malaysia</option>
            <option value="">Filiphina</option>
            <option value="">India</option>
        </select>

        <br><br>
        <label for="">Language Spoken :</label>
        <br><br>
        <input type="checkbox" name="indo" id="">
        <label for="">Bahasa Indonesia</label>
        <br>
        <input type="checkbox" name="eng" id="">
        <label for="">English</label>
        <br>
        <input type="checkbox" name="other" id="">
        <label for="">Other</label>

        <br><br>
        <label for="">Bio :</label>
        <br><br>
        <textarea name="bio" id="" cols="40" rows="10"></textarea>
        <br><br>
        <button type="submit">Sign Up</button>
    </form>
</body>

</html>
